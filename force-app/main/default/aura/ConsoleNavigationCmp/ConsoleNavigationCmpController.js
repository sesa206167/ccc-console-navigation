({
  handleNavigationEvent: function(component, evt) {
    // Get details from the DOM event fired by the Lightning web component
    console.log("am inside handle NavigationEvent");
    var workspaceAPI = component.find("workspace");
    var pagedata = evt.getParam("navigationparameters");
    console.log(pagedata);
    workspaceAPI
      .openTab({
        pageReference: pagedata.primaryTab
      })
      .then(function(tabId) {
        // var pagedata = JSON.parse(component.get('v.pagedata'));
        if (pagedata.hasOwnProperty("subTab")) {
          return Promise.all(
            pagedata.subTab.map(function(eachSubTab) {
              return workspaceAPI.openSubtab({
                parentTabId: tabId,
                pageReference: eachSubTab
              });
            })
          );
        }
      });
    // .catch();
  }
});
