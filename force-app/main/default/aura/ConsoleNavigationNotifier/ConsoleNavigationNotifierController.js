({
  doInit: function(component, event, helper) {
    // note different syntax for getting application event
    var appEvent = $A.get("e.c:ConsoleNavigation");
    appEvent.setParams({
      navigationparameters: {
        pagedata: {
          primaryTab: {
            type: "standard__recordPage",
            attributes: {
              recordId: "500e000000E5MlNAAV",
              actionName: "view"
            }
          }
        }
      }
    });
    appEvent.fire();
  }
});
