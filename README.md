# Workflow

List view button is redirecting to a visualforce page. We identify the container type in visualforce and raise a console navigation event (aura:application Event) which will be captured by a ConsoleNavigationCmp.

ConsoleNavigationCmp will capture the event and parse the navigationparameters and call the workspace api to perform the redirect.

## Navigate using workspace

```js
        primaryTab: {
                type: 'standard__recordPage',
                attributes: {
                    recordId: '500e000000E5MlNAAV',
                    actionName: 'view',
                },
            }
```

## Resources

## Description of Files and Directories

## Issues
